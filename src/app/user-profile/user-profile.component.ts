import { Component, OnInit } from '@angular/core';
import { GlobalService } from "../global.service";
import { Router, ActivatedRoute } from '@angular/router';
import { ServerService } from "../server.service";
import 'rxjs/add/operator/map'
import { Http, Response } from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'cf-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.less']
})
export class UserProfileComponent implements OnInit {

  userEmail:any;
  userPassword:any="";
  userPasswordConfirm:any="";
  userName:any;
  userBio:any;
  userLocation:any;
  totalProjectFunded:any;
  totalProjectRaised:any;
  totalFundedAmount:any;

  constructor(
      private global : GlobalService,
      private router: Router,
      private server : ServerService,
      private http: Http, 
      private httpClient: HttpClient
    ) { }

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);

    if(this.global.userName == ""){
      this.router.navigate(['../home']);
    }else{
      var userData = JSON.parse(localStorage.getItem("userData"));
      this.global.userToken = userData.token;
      this.global.userEmail = userData.email;

      this.getUserProfile();
      
    }

  }

  getUserProfile(){

    const userData = { "USER_EMAIL" :  this.global.userEmail };

    this.server.getUserProfile(userData, this.global.userToken).subscribe(getProfileData => {
          
        if(getProfileData.status == true){
          this.global.usedProfileData = getProfileData;
          this.global.userName = getProfileData.USER_NAME;
          this.userEmail = this.global.userEmail;
          this.userName = getProfileData.USER_NAME;
          this.userBio = getProfileData.USER_BIO;
          this.userLocation =  getProfileData.USER_LOCATION;
          this.totalProjectFunded = getProfileData.TOTAL_PROJECTS_FUNDED;
          this.totalProjectRaised = getProfileData.TOTAL_PROJECTS_RAISED;
          this.totalFundedAmount = getProfileData.TOTAL_FUNDED_AMOUNT;

          var userData = {
            'name' : getProfileData.USER_NAME,
            'email' : getProfileData.USER_EMAIL,
            'contact': getProfileData.USER_CONACT,
            'role': getProfileData.USER_ROLE,
            'status' : getProfileData.USER_STATUS,
            'token' : this.global.userToken,
            'location' : getProfileData.USER_LOCATION,
            'bio': getProfileData.USER_BIO
          }

          localStorage.setItem("userData", JSON.stringify(userData));
          

        }else{
          console.log("something went wrong");
        }

    });

  }


}
