import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalService } from "../global.service";
import { ServerService } from "../server.service";
import 'rxjs/add/operator/map'
import { Http, Response } from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { HttpParams } from "@angular/common/http";

import { AuthService } from '../services/auth.service';
import { PersistenceService } from 'angular-persistence';

@Component({
  selector: 'cf-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  loginView :boolean = true;
  userEmail:any="";
  userPassword:any="";
  invalidCreds:Boolean=false;
  


  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private authService: AuthService,
      private global : GlobalService, 
      private server : ServerService,
      private http: Http, 
      private httpClient: HttpClient,
      private persistenceService: PersistenceService
  ) {}

  ngOnInit() {
      this.authService.removeHeader();
      this.loginForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
      });
      // let StorageType

      

      
  }
  resetForm(){
    this.loginForm.reset();
    // this.registerForm.reset();
  }

  get loginControl(){
    return this.loginForm.controls; 
  }

  

  onLogin(){
      this.submitted = true;
      console.log("valid")
      this.invalidCreds = false;
      if (this.userEmail != "" && this.userPassword != "") {
        this.loading = true;
        
        const userData = { "USER_EMAIL" : this.userEmail, "USER_PASSWORD" : this.userPassword }
        this.server.getUserSignin(userData).subscribe(getSigninResult => {
          
          if(getSigninResult.status == true){
            console.log("successful signup");

            this.global.userToken = getSigninResult.token;
            this.global.userName = getSigninResult.USER_NAME;
            this.global.userEmail = this.userEmail;
            this.global.userID = getSigninResult.USER_ID;
            this.loading = false;
            this.invalidCreds = false;
            this.getUserProfile();
          }else{
            console.log("something went wrong")
            this.loading = false;
            this.invalidCreds = true;
          }
   
        });
      }
  }

  getUserProfile(){

    const userData = { "USER_EMAIL" : this.userEmail };

    this.server.getUserProfile(userData, this.global.userToken).subscribe(getProfileData => {
          
        if(getProfileData.status == true){
          this.loading = false;
          this.global.usedProfileData = getProfileData;
          this.global.userName = getProfileData.USER_NAME;
          var userData = {
            'name' : getProfileData.USER_NAME,
            'email' : getProfileData.USER_EMAIL,
            'contact': getProfileData.USER_CONACT,
            'role': getProfileData.USER_ROLE,
            'status' : getProfileData.USER_STATUS,
            'token' : this.global.userToken,
            'location' : getProfileData.USER_LOCATION,
            'bio': getProfileData.USER_BIO
          }

          localStorage.setItem("userData", JSON.stringify(userData));
          console.log("nameeeeeeeeeeeeeeeeee ",localStorage.getItem("userData"));


          this.router.navigate(['../home']);

        }else{
          console.log("something went wrong");
          this.loading = false;
        }

    });

  }

  openRegisterForm(){
    document.getElementById('loginFormDiv').style.display = "none";
    document.getElementById('loginFormDiv').style.display = "none";
  }

  forgotCredential(){
    console.log("forgotCredential()");
  }
}