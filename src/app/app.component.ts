import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'cf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'fund-raiser';

}

