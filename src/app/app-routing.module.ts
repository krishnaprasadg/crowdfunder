import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SupportComponent } from './support/support.component';
import { HomeComponent } from './home/home.component';
import { StartProjectComponent } from './start-project/start-project.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { SignupComponent } from './signup/signup.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { TrustSafetyComponent  } from './trust-safety/trust-safety.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';
import { PressComponent } from './press/press.component';
import { FundDetailComponent } from './fund-detail/fund-detail.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'support', component: SupportComponent },
    { path: 'start-project', component: StartProjectComponent },
    { path: 'create-project', component: CreateProjectComponent },
    { path: 'about', component: AboutusComponent},
    { path: 'trust-safety', component: TrustSafetyComponent },
    { path: 'what-we-do', component: WhatWeDoComponent },
    { path: 'press', component: PressComponent },
    { path: 'fund-detail', component: FundDetailComponent },
    { path: 'profile', component: UserProfileComponent},
    { path: 'settings', component: UserSettingsComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
