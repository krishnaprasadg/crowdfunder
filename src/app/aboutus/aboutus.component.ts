import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cf-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.less']
})
export class AboutusComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);
  }
}
