import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cf-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.less']
})
export class SupportComponent implements OnInit {
  
  constructor() { }

  isBankerCollapsed: boolean;
  isBankerCollapsed1: boolean;
  isBankerCollapsed2: boolean;

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);
    this.isBankerCollapsed = true;
    this.isBankerCollapsed1 = true;
    this.isBankerCollapsed2 = true;
  }

  openSupport(selection : any){
    console.log(selection);
    document.getElementById('support-main').style.display="none";
    document.getElementById('faq-div').style.display="block";
    document.getElementById(selection).style.display="block";
  }

  goBack(){
    document.getElementById('support-main').style.display="block";
    document.getElementById('faq-div').style.display="none";
    document.getElementById('banker').style.display="none";
    document.getElementById('payment').style.display="none";
    document.getElementById('profile').style.display="none";
  }
  
  changeIcon(ID:any,status:Boolean){
    console.log(ID)
    if(!status) {
      console.log("if")
      document.getElementById(ID).classList.remove("fa-angle-down");
      document.getElementById(ID).classList.add("fa-angle-up");
    } else {
      console.log("else")
      document.getElementById(ID).classList.add("fa-angle-down");
      document.getElementById(ID).classList.remove("fa-angle-up");
    }
  }

}
