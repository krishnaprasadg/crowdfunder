import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { GlobalService } from "../global.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cf-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  loginView$: Observable<boolean>;

  constructor(private authService: AuthService,private global : GlobalService,private router: Router,) { }

  ngOnInit() {
    this.loginView$ = this.authService.inLoginView;
    console.log(this.global.userName);
    var userData =   JSON.parse(localStorage.getItem("userData"));

    if( localStorage.getItem("userData") != null ){
      this.global.userName =  userData.name;
      this.global.userEmail =   userData.email;
      this.global.user_contact = userData.contact;
      this.global.userRole = userData.role;
      this.global.userStatus = userData.status;
      this.global.userToken = userData.token;
      this.global.location = userData.location;
      this.global.bio = userData.bio;
      
    }else{
      this.global.userName = "";
      this.global.userEmail = "";
      this.global.user_contact = "";
      this.global.userRole = "";
      this.global.userStatus = "";
      this.global.userToken = "";
      this.global.location = "";
      this.global.bio = "";

    }
    
  }

  logout(){
    localStorage.removeItem("userData");
    this.global.userName = "";
    this.global.userEmail = "";
    this.global.user_contact = "";
    this.global.userRole = "";
    this.global.userStatus = "";
    this.global.userToken = "";
    this.global.location = "";
    this.global.bio = "";
    this.router.navigate(['../home']);
  }

}
