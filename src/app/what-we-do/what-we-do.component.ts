import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cf-what-we-do',
  templateUrl: './what-we-do.component.html',
  styleUrls: ['./what-we-do.component.less']
})
export class WhatWeDoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);
  }

}
