import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GlobalService } from "../global.service";
import { Router } from '@angular/router';

@Component({
  selector: 'cf-start-project',
  templateUrl: './start-project.component.html',
  styleUrls: ['./start-project.component.less']
})
export class StartProjectComponent implements OnInit {
  projectCategory:any;
  project:any;
  openSec:Boolean = false;
  typeOfAcnt:any;
  projDesc:any="";
  projLocation:any="";
  lastCategory:any = null;
  startProjError:Boolean=false;

  constructor( private router: Router,
    private global : GlobalService, private cdr: ChangeDetectorRef) { }

  ngAfterViewChecked(){
      //your code to update the model
      this.cdr.detectChanges();
  }

  ngOnInit() {

  }

  openSection(){
    document.getElementById('start-section-1').style.display = "none";
    document.getElementById('start-section-2').style.display = "block";
    $('html, body').animate({
        scrollTop: $('body').offset().top
    }, 800);
  }


  startProject(category){
    this.projectCategory = category;
    this.openSec = true;
    category = category.split(' ').join('');
    if(this.lastCategory != null){
      document.getElementById(this.lastCategory).style.color = "#ffffff";
    }
    document.getElementById(category).style.color = "#9013FE";
    this.lastCategory = category;
  }

  setProjLocation(location){
    this.projLocation = location;
    document.getElementById('locationDrowdown').innerText = location;
  }
  setAcntType(acnt){
    this.typeOfAcnt = acnt;
  }

  createProject(){

    this.global.projectCategory = this.projectCategory;
    this.global.typeOfAcnt = this.typeOfAcnt;
    this.global.projDesc = this.projDesc;
    this.global.projLocation = this.projLocation;
    
    if(this.global.userEmail != "" && this.global.userToken != ""){
      if(this.projDesc != "" && this.projLocation != ""){
        this.startProjError = false;
        this.router.navigate(['../create-project']);
      }else{
        this.startProjError = true;
      }
     
    }else{
      this.router.navigate(['../login']);
    }
    
    
  }

}
