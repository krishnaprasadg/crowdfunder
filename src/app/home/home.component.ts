import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { GlobalService } from "../global.service";

@Component({
  selector: 'cf-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService,private global : GlobalService) { }

  ngOnInit() {
    this.authService.addHeader();
    console.log(this.global.userName);
  }

}
