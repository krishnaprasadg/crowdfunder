import { Component, OnInit } from '@angular/core';
import { GlobalService } from "../global.service";
import { Router, ActivatedRoute } from '@angular/router';
import { ServerService } from "../server.service";
import 'rxjs/add/operator/map'
import { Http, Response } from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'cf-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.less']
})
export class UserSettingsComponent implements OnInit {
  isAddNewCardCollapsed: boolean;
  userEmail:any;
  userPassword:any="";
  userPasswordConfirm:any="";
  userName:any;
  userBio:any;
  userLocation:any;
  userSavedCard:any="";
  userNewCardNumber:any="";
  userNewCardExp:any="";
  userNewCardZipcode:any="";
  userNewCardName:any="";
  userNewCardSecurityCode:any="";
  passowrdMismatchStatus:boolean=false;
  passowrdErrorStatus:boolean=false;
  passwordSuccessStatus:boolean=false;
  passwordApiErrStatus:boolean=false;
  profileErrStatus:boolean=false;
  profileSuccessStatus:boolean=false;
  cardValidationErrStatus:boolean=false;
  cardErrStatus:boolean=false;
  cardSuccessStatus:boolean=false;

  constructor(private global : GlobalService,
    private router: Router,
    private server : ServerService,
      private http: Http, 
      private httpClient: HttpClient) { }

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);

    this.isAddNewCardCollapsed = true;

    

    if(this.global.userName == ""){
      this.router.navigate(['../home']);
    }else{
      var userData = JSON.parse(localStorage.getItem("userData"));
      this.global.userToken = userData.token;
      this.global.userEmail = userData.email;

      this.getUserProfile();
      
    }


  }

  getUserProfile(){

    const userData = { "USER_EMAIL" :  this.global.userEmail };

    this.server.getUserProfile(userData, this.global.userToken).subscribe(getProfileData => {
          
        if(getProfileData.status == true){
          this.global.usedProfileData = getProfileData;
          this.global.userName = getProfileData.USER_NAME;
          this.userEmail = this.global.userEmail;
          this.userName = getProfileData.USER_NAME;
          this.userBio = getProfileData.USER_BIO;
          this.userLocation =  getProfileData.USER_LOCATION;

          var userData = {
            'name' : getProfileData.USER_NAME,
            'email' : getProfileData.USER_EMAIL,
            'contact': getProfileData.USER_CONACT,
            'role': getProfileData.USER_ROLE,
            'status' : getProfileData.USER_STATUS,
            'token' : this.global.userToken,
            'location' : getProfileData.USER_LOCATION,
            'bio': getProfileData.USER_BIO
          }

          localStorage.setItem("userData", JSON.stringify(userData));
          

        }else{
          console.log("something went wrong");
        }

    });

  }

  addNewCard() {
    if(this.isAddNewCardCollapsed) {
      document.getElementById("collapse-addnew").innerHTML = "-";
    } else {
      document.getElementById("collapse-addnew").innerHTML = "+";
    }
    this.isAddNewCardCollapsed = !this.isAddNewCardCollapsed;
  }

  removeErrorState(){
    const passwordField = < HTMLButtonElement > document.getElementById('newPassword');
    passwordField.style.border = '1px solid #979797';
    const passwordFieldConfirm = < HTMLButtonElement > document.getElementById('newPasswordConfirm');
    passwordFieldConfirm.style.border = '1px solid #979797';
  }

  updatePwd(){
    if(this.userPassword === this.userPasswordConfirm){

      if(this.userEmail != "" && this.userPassword != "" && this.userPasswordConfirm != ""){
      
          const userData = { "action" : "update_pwd", "USER_EMAIL" : this.userEmail, "USER_PASSWORD" : this.userPassword };
          this.server.setUserNewPassword(userData, this.global.userToken).subscribe(getChangePasswordResult => {
            
            console.log(getChangePasswordResult);
            if(getChangePasswordResult.status == true){

                this.passwordSuccessStatus = true;
                setTimeout(() => {
                  this.passwordSuccessStatus = false;
                }, 3000);

            }else{

                this.passwordApiErrStatus = true;
                setTimeout(() => {
                  this.userPassword = "";
                  this.userPasswordConfirm = "";
                  this.passwordApiErrStatus = false;
                }, 3000);

            }
    
          });
      }else{

          this.passowrdErrorStatus = true;
          document.getElementById('newPassword').style.border = '1px solid #FD4545';
          document.getElementById('newPasswordConfirm').style.border = '1px solid #FD4545';
          setTimeout(() => {
              this.passowrdErrorStatus = false;
          }, 3000);

      }
    }else{

      this.passowrdMismatchStatus = true;
      document.getElementById('newPassword').style.border = '1px solid #FD4545';
      document.getElementById('newPasswordConfirm').style.border = '1px solid #FD4545';
      setTimeout(() => {
          this.passowrdMismatchStatus = false;
      }, 3000);
      
    }

  }


  updateProfileData(){
    
    const userData = { "action" : "update_profile", "USER_EMAIL" : this.userEmail, "USER_NAME" : this.userName, "USER_LOCATION" : this.userLocation, "USER_BIO" : this.userBio };
    this.server.setUserNewPassword(userData, this.global.userToken).subscribe(getUserProfileResult => {
      if(getUserProfileResult.status == true){
        this.profileSuccessStatus = true;
        setTimeout(() => {
          this.profileSuccessStatus = false;
        }, 3000);
      }else{
        this.profileErrStatus = true;
        setTimeout(() => {
          this.profileErrStatus = false;
        }, 3000);
      }
    });
  }

  updateCardData(){

    if(this.userNewCardNumber != "" && this.userNewCardName != "" && this.userNewCardExp != ""){

      const cardData = { "action" : "add", "USER_EMAIL" : this.userEmail, "CARD_NUMBER" : this.userNewCardNumber, "CARD_HOLDER_NAME" : this.userNewCardName, "EXPIRATION" : this.userNewCardExp };
      this.server.setUserCardData(cardData, this.global.userToken).subscribe(getCardDetails => {

        if(getCardDetails.status == true){
          this.cardSuccessStatus = true;
          setTimeout(() => {
            this.cardSuccessStatus = false;
          }, 3000);
        }else{
          this.cardErrStatus = true;
          setTimeout(() => {
            this.cardErrStatus = false;
          }, 3000);
        }
        
      });

    }else{
      this.cardValidationErrStatus = true;
        setTimeout(() => {
          this.cardValidationErrStatus = false;
        }, 3000);
    }
    
  }


  

}
