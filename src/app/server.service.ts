import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  headers: any;
  options: any;

  constructor(private http: Http, private global: GlobalService) { }

  private jwt() {
    // create authorization header with jwt token
    this.headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    return new RequestOptions({ headers: this.headers });
  }
  private jwt_tokenizer(token) {
    // create authorization header with jwt token
    this.headers = new Headers({ 'Authorization' : token ,'Content-Type': 'application/x-www-form-urlencoded'});
    console.log(this.headers)
    return new RequestOptions({ headers: this.headers });
  }


  getUserSignup(userData: any) {
    const url = this.global.baseURL + this.global.signUp;
    return (this.http.post(url, JSON.stringify(userData), this.jwt()).map((response: Response) => response.json()));
  }

  getUserSignin(userData: any) {
    const url = this.global.baseURL + this.global.signIn;
    return (this.http.post(url, JSON.stringify(userData), this.jwt()).map((response: Response) => response.json()));
  }
  
  getUserProfile(userData: any, token) {
    const url = this.global.baseURL + this.global.userProfile;
    console.log(userData)
    return (this.http.post(url, JSON.stringify(userData), this.jwt_tokenizer(token)).map((response: Response) => response.json()));
  }

  setUserNewPassword(userData: any, token:any) {
    const url = this.global.baseURL + this.global.userDataUpdate;
    return (this.http.post(url, JSON.stringify(userData), this.jwt_tokenizer(token)).map((response: Response) => response.json()));
  }

  setUserCardData(cardData: any, token:any) {
    console.log(cardData)
    const url = this.global.baseURL + this.global.userCardDetails;
    return (this.http.post(url, JSON.stringify(cardData), this.jwt_tokenizer(token)).map((response: Response) => response.json()));
  }

  setCreateProject(createData: any, token:any) {
    const url = this.global.baseURL + this.global.createProject;
    console.log(createData)
    return (this.http.post(url, JSON.stringify(createData), this.jwt_tokenizer(token)).map((response: Response) => response.json()));
  }

  setaddCollaborator(collaboratorData: any, token: any) {
    const url = this.global.baseURL + this.global.addCollaborator;
    console.log(collaboratorData)
    return (this.http.post(url, JSON.stringify(collaboratorData), this.jwt_tokenizer(token)).map((response: Response) => response.json()));
  }


}
