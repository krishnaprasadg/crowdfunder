import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cf-trust-safety',
  templateUrl: './trust-safety.component.html',
  styleUrls: ['./trust-safety.component.less']
})
export class TrustSafetyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);
  }

}
