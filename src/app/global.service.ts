import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  baseURL:any;
  signUp:any;
  signIn:any;
  userProfile:any;
  usedProfileData:any;
  userDataUpdate:any;
  userCardDetails:any;
  userName:any;
  userEmail:any;
  userToken:any;
  userID:any;
  userRole:any;
  userStatus:any = 0;
  total_funded_amount:Number = 0;
  total_funded_project:Number = 0;
  total_project_raised:Number = 0;
  user_contact:any = 0;
  location:any;
  bio:any;
  createProject:any;
  addCollaborator:any;

  // create project global values
  projectCategory:any;
  typeOfAcnt:any;
  projDesc:any;
  projLocation:any;

  constructor() {
    this.baseURL = "http://13.233.40.118/api/";
    this.signUp = "user/signup";
    this.signIn = "user/signin";
    this.userProfile = "user/profile";
    this.userDataUpdate = "user/update";
    this.userCardDetails = "user/card";
    this.createProject = "project/create";
    this.addCollaborator = "project/addcbr";
    this.usedProfileData = "";
    this.userRole = "";
    this.userEmail = "";
    this.userID = "";
    this.userName = "";
    this.location = "";
    this.bio = "";
   }
}
