import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cf-press',
  templateUrl: './press.component.html',
  styleUrls: ['./press.component.less']
})
export class PressComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);
  }

}
