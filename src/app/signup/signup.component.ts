import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalService } from "../global.service";
import { ServerService } from "../server.service";
import 'rxjs/add/operator/map'
import { Http, Response } from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { HttpParams } from "@angular/common/http";
import {Router} from '@angular/router';

@Component({
  selector: 'cf-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.less']
})
export class SignupComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  loading = false;
  userFullname:any="";
  userEmail:any="";
  userPassword:any="";

  constructor(private formBuilder: FormBuilder, private router: Router,private authService: AuthService, private global : GlobalService, private server : ServerService,private http: Http, private httpClient: HttpClient) { }

  ngOnInit() {
    this.authService.removeHeader();
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]]
  });
  }

  get registerControl(){ 
    return this.registerForm.controls; 
  }


  onRegister() {
    this.submitted = true;

    if (this.userFullname != "" && this.userEmail != "" && this.userPassword != "") {
      this.loading = true;
      console.log("valid")
      const userData = { "USER_NAME" : this.userFullname, "USER_EMAIL" : this.userEmail, "USER_PASSWORD" : this.userPassword }
      this.server.getUserSignup(userData).subscribe(getSignupResult => {
        
        if(getSignupResult.status == true){
          console.log("successful signup");
          this.loading = false;
          this.router.navigate(['../login']);
        }else{
          console.log("something went wrong")
          this.loading = false;
        }
 
      });

    }else{

    }
    
  }

}
