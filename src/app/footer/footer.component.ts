import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'cf-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {

  loginView$: Observable<boolean>;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.loginView$ = this.authService.inLoginView;
  }

}
