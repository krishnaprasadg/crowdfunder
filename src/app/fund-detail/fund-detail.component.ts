import { Component, OnInit } from '@angular/core';
import { GlobalService } from "../global.service";
import { Router, ActivatedRoute } from '@angular/router';
import { ServerService } from "../server.service";
import 'rxjs/add/operator/map'
import { Http, Response } from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'cf-fund-detail',
  templateUrl: './fund-detail.component.html',
  styleUrls: ['./fund-detail.component.less']
})
export class FundDetailComponent implements OnInit {

  isFAQCollapsed : any;
  isFAQCollapsed1 : any;
  isFAQCollapsed2 : any;
  userName:any;
  userEmail:any;
  userBackedProjects:any=0;
  userRaisedProject:any=0;

  constructor(private global : GlobalService,
      private router: Router,
      private server : ServerService,
      private http: Http, 
      private httpClient: HttpClient) { }

  ngOnInit() {
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);
    this.isFAQCollapsed = false;
    this.isFAQCollapsed1 = true;
    this.isFAQCollapsed2 = true;

    if(this.global.userName == ""){
      this.router.navigate(['../home']);
    }else{
      var userData = JSON.parse(localStorage.getItem("userData"));
      this.global.userToken = userData.token;
      this.global.userEmail = userData.email;
      this.userName =this.global.userName;
      this.userBackedProjects = this.global.total_funded_project;
      this.userRaisedProject = this.global.total_project_raised;
      
    }

  }
  
  bgChange(ID:any, status:Boolean){
    console.log(ID+"-id");
    console.log(status)
    if(!status){
      document.getElementById(ID+"-id").style.backgroundColor = "#D8D8D8";
    }else{
      document.getElementById(ID+"-id").style.backgroundColor = "initial";
    }
    
  }

}
