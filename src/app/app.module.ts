import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { HashLocationStrategy, PathLocationStrategy, LocationStrategy, CommonModule, APP_BASE_HREF } from '@angular/common';
import * as $ from 'jquery';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { PersistenceModule } from 'angular-persistence';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SupportComponent } from './support/support.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';

import { StartProjectComponent } from './start-project/start-project.component';
import { CreateProjectComponent } from './create-project/create-project.component';

import { AuthService } from './services/auth.service';
import { SignupComponent } from './signup/signup.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AboutusComponent } from './aboutus/aboutus.component';
import { TrustSafetyComponent } from './trust-safety/trust-safety.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';
import { PressComponent } from './press/press.component';
import { FundDetailComponent } from './fund-detail/fund-detail.component';
import { ArtBoardComponent } from './art-board/art-board.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NumericOnlyDirective } from './numeric-only.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SupportComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    StartProjectComponent,
    CreateProjectComponent,
    SignupComponent,
    AboutusComponent,
    TrustSafetyComponent,
    WhatWeDoComponent,
    PressComponent,
    FundDetailComponent,
    ArtBoardComponent,
    UserProfileComponent,
    UserSettingsComponent,
    NumericOnlyDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    CollapseModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    NgxDropzoneModule,
    PersistenceModule,
    PopoverModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  providers: [
    AuthService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
