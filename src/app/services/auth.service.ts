import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class AuthService {
  private loginView: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  get inLoginView() {
    return this.loginView.asObservable();
  }
  constructor() { }

  removeHeader() {
    this.loginView.next(false);
  }

  addHeader() {
    this.loginView.next(true);
  }
}
