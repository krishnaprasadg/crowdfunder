import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GlobalService } from "../global.service";
import { ServerService } from "../server.service";
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'cf-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.less']
})
export class CreateProjectComponent implements OnInit {
  projTitle:any="";
  projSubTitle:any="";
  projCategory:any;
  projCurrency:any="";
  projFundingGoalAmount:any="";
  projDescription:any="";
  projChallenges:any="";
  perkTitle:any="";
  perkAmount:any="";
  perkDesc:any="";
  collaboratorEmail:any="";
  IdentityFirstName:any="";
  IdentityLastName:any="";
  DOBDay:any="";
  DOBMonth:any="";
  DOBYear:any="";
  addressOne:any="";
  addressTwo:any="";
  addressCity:any="";
  addressPin:any="";
  addressCountry:any="";
  fundOwner:any = "Organisation";
  campainDuration:any;
  perkQuantity:any = "Limited";
  CollabSuccessMsg:Boolean=false;
  invalidCollabMsg:Boolean=false;

  invalidBasics:Boolean=false;  
  invalidStory:Boolean=false;
  invalidPerk:Boolean=false;
  invalidPeople:Boolean=false;
  invalidPayment:Boolean=false;
  allCollaborators:any=[];

  bsValue = {
    dateInputFormat: 'YYYY-MM-DD',
    isAnimated: true,
    adaptivePosition: true,
    minDate : new Date(),
    maxDate : new Date(moment(new Date()).add(60, 'days').format('YYYY-MM-DD'))
  }

  constructor(private router: Router, public global : GlobalService, private server: ServerService, private cdr: ChangeDetectorRef) { }
  ngAfterViewChecked(){
      //your code to update the model
      this.cdr.detectChanges();
  }
  ngOnInit() {
    
    if(this.global.userEmail != "" && this.global.userToken != ""){
      this.router.navigate(['../create-project']);
    }else{
      this.router.navigate(['../login']);
    }
    
    if(this.global.projectCategory != undefined){
      document.getElementById('projCategory').innerText = this.global.projectCategory;
      this.projCategory = this.global.projectCategory;
    }
    
    $('html, body').animate({
      scrollTop: $('body').offset().top
    }, 800);

    this.projDescription = this.global.projDesc;

  }

  nextSection(openDiv, newDiv){
    
   if(openDiv == "basics"){
     
    if(this.projTitle != "" && this.projSubTitle != "" && this.projCategory != "" && this.projFundingGoalAmount != ""){
      
      $('html, body').animate({
        scrollTop: $('body').offset().top
        }, 300);
        $("#"+openDiv).hide(500);
        $('#'+openDiv+"-anchor").addClass('sidenav-text');
        $('#'+openDiv+"-anchor").removeClass('sidenav-active');
        $('#'+newDiv+"-anchor").addClass('sidenav-active');
        $('#'+newDiv+"-anchor").removeClass('sidenav-text');
        $("#"+newDiv).show(500);
        this.invalidBasics = false;
    }else{
      this.invalidBasics = true; 
    }

   }else if(openDiv == "story"){

    if(this.projDescription != "" && this.projChallenges != ""){
      $('html, body').animate({
        scrollTop: $('body').offset().top
        }, 300);
        $("#"+openDiv).hide(500);
        $('#'+openDiv+"-anchor").addClass('sidenav-text');
        $('#'+openDiv+"-anchor").removeClass('sidenav-active');
        $('#'+newDiv+"-anchor").addClass('sidenav-active');
        $('#'+newDiv+"-anchor").removeClass('sidenav-text');
        $("#"+newDiv).show(500);
        this.invalidStory = false;
    }else{
      this.invalidStory = true; 
    }

   }else if(openDiv == "perks"){

    if(this.perkDesc != "" && this.perkAmount != "" && this.perkTitle != ""){
      $('html, body').animate({
        scrollTop: $('body').offset().top
        }, 300);
        $("#"+openDiv).hide(500);
        $('#'+openDiv+"-anchor").addClass('sidenav-text');
        $('#'+openDiv+"-anchor").removeClass('sidenav-active');
        $('#'+newDiv+"-anchor").addClass('sidenav-active');
        $('#'+newDiv+"-anchor").removeClass('sidenav-text');
        $("#"+newDiv).show(500);
        this.invalidPerk = false;
    }else{
      this.invalidPerk = true; 
    }

   }else if(openDiv == "payment"){

    if(this.IdentityFirstName != "" && this.IdentityLastName != "" && this.DOBDay != "" && this.DOBMonth != "" && this.DOBYear != "" && this.addressOne != "" && this.addressCity != "" && this.addressPin != "" && this.addressCountry != ""){
      
      this.invalidPayment = false;

      //
      this.createProject();
       
      // 

    }else{
      this.invalidPayment = true; 
    }

   }else{ 
      $('html, body').animate({
      scrollTop: $('body').offset().top
      }, 300);
      $("#"+openDiv).hide(500);
      $('#'+openDiv+"-anchor").addClass('sidenav-text');
      $('#'+openDiv+"-anchor").removeClass('sidenav-active');
      $('#'+newDiv+"-anchor").addClass('sidenav-active');
      $('#'+newDiv+"-anchor").removeClass('sidenav-text');
      $("#"+newDiv).show(500);
    }

  }
  openSection(openDiv){
    $("#rules").hide(500);
    $("#basics").hide(500);
    $("#story").hide(500);
    $("#perks").hide(500);
    $("#people").hide(500);
    $("#payment").hide(500);
    $('#rules-anchor').addClass('sidenav-text');
    $('#rules-anchor').removeClass('sidenav-active');
    $('#basics-anchor').addClass('sidenav-text');
    $('#basics-anchor').removeClass('sidenav-active');
    $('#story-anchor').addClass('sidenav-text');
    $('#story-anchor').removeClass('sidenav-active');
    $('#perks-anchor').addClass('sidenav-text');
    $('#perks-anchor').removeClass('sidenav-active');
    $('#people-anchor').addClass('sidenav-text');
    $('#people-anchor').removeClass('sidenav-active');
    $('#payment-anchor').addClass('sidenav-text');
    $('#payment-anchor').removeClass('sidenav-active');

    
    $('#'+openDiv+"-anchor").removeClass('sidenav-text');
    $('#'+openDiv+"-anchor").addClass('sidenav-active');
    $("#"+openDiv).show(500);
    
    
  }
  setPerkQuantity(quant:any){
    this.perkQuantity = quant;
  }
  setFundOwner(own:any){
    this.fundOwner = own;
  }

  onFilesAdded(files: File[]) {
   
    files.forEach(file => {
      const reader = new FileReader();
   
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        // this content string could be used directly as an image source
        // or be uploaded to a webserver via HTTP request.
      };
      // use this for basic text files like .txt or .csv
      reader.readAsText(file);
      // use this for images
      // reader.readAsDataURL(file);
    });
  }

  setProjCat(category:any){
    this.projCategory = category;
    document.getElementById('projCategory').innerText = category;
  }

  setCurr(currency:any){
    this.projCurrency = currency;
    document.getElementById('currenyDiv').innerText = currency;
  }

  addCollaborator() {
    this.invalidCollabMsg = false;
    this.CollabSuccessMsg = false;
    if(this.collaboratorEmail != "" && this.collaboratorEmail != null && this.collaboratorEmail != undefined){
      const collaboratorData = {
        "PROJECT_SHARE_USERID" : this.collaboratorEmail,
        "PROJECT_SHARE_VIEW" : "YES",
        "PROJECT_SHARE_EDIT" : "NO",
        "PROJECT_SHARE_DELETE" : "NO",
        "PROJECT_SHARE_SHARED_BY" : this.global.userEmail /* Logged in email/username */
      };
      
      this.server.setaddCollaborator(collaboratorData, this.global.userToken).subscribe(getCollaborator => {
        console.log(getCollaborator);
        this.CollabSuccessMsg = true;
        if(getCollaborator.status == true){
          this.allCollaborators.push(this.collaboratorEmail);
        }
        
        this.collaboratorEmail = '';
        
      });
    }else{
      this.invalidCollabMsg = true;
    }

  }

  createProject(){

      const createProjectData = { 
        "PROJECT_NAME" : this.projTitle,
        "PROJECT_SUB_NAME" : this.projSubTitle,
        "PROJECT_CATEGORY" : this.projCategory,
        "PROJECT_IMAGES" : "", 
        "PROJECT_VIDEOS" : "",
        "PROJECT_FUNDING_GOAL" : this.projFundingGoalAmount,
        "PROJECT_FUNDING_CURRENCY" : this.projCurrency,
        "PROJECT_START_DATE" : moment(this.campainDuration[0]).format('YYYY-MM-DD'),
        "PROJECT_END_DATE" : moment(this.campainDuration[1]).format('YYYY-MM-DD'),
        "PROJECT_DESCRIPTION" : this.projDescription,
        "PROJECT_CHALLEGES" : this.projChallenges,
        "PROJECT_LOCATION" : this.global.projLocation,
        "PROJECT_TYPE" : this.global.typeOfAcnt,
        "PROJECT_FUNDS_RECIPIENT" : this.fundOwner.toUpperCase(),
        "PERKS_TITLE" : this.perkTitle,
        "PERKS_FUNDS" : this.perkAmount,
        "PERKS_DESCRIPTION" : this.perkDesc,
        "PERKS_TARGET_DATE" : moment(this.campainDuration[1]).format('YYYY-MM-DD'),
        "MAX_PARTICITANTS" : "0",
        "FIRST_NAME" : this.IdentityFirstName,
        "LAST_NAME": this.IdentityLastName,
        "DATE_OF_BIRTH" : this.DOBDay + '-' + this.DOBMonth + '-' + this.DOBYear,
        "HOME_ADDRESS" : this.addressOne + " " + this.addressTwo,
        "CITY" : this.addressCity,
        "POSTAL_CODE" : this.addressPin,
        "COUNTRY" : this.addressCountry,
        "BANK_NAME" : "",
        "BANK_ACC_NO" : "",
        "IFSC_CODE" : ""
     };
  
      this.server.setCreateProject(createProjectData, this.global.userToken).subscribe(getCreateData => {
        console.log(getCreateData)
      });
  
    // }
  }

  removeCollab(mail:any){
    console.log("mail to be removed from collab is ", mail);
    var index =  this.allCollaborators.indexOf(mail);
    this.allCollaborators.splice(index, 1);
  }

  

}
